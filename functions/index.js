const functions = require('firebase-functions');
const admin = require('firebase-admin');

const config = {
    apiKey: "AIzaSyCp9QzLbkbT6MUwU807kr_R2gf_Y3-Rssk",
    authDomain: "notas-365.firebaseapp.com",
    databaseURL: "https://notas-365.firebaseio.com",
    projectId: "notas-365",
    storageBucket: "notas-365.appspot.com",
    messagingSenderId: "740723294888"
};

admin.initializeApp(config);

const database = admin.firestore()

exports.createUser = functions.auth.user().onCreate(user => {
    const { uid, displayName, photoURL, email } = user;
    return database.collection('users').doc(uid)
        .set({ user: { uid, displayName, email, photoURL } });
});

exports.deleteUser = functions.auth.user().onDelete(user => {
    const { uid } = user;
    return database.collection('users').doc(uid).delete()
});
