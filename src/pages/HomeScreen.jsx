import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { user } from '../state/mapStates/authState';

import { StyledButton } from '../styled-components/buttons';
import HomeNav from '../containers/HomeNav';
import { LoginButton, LinkToLoginWithEmailButton } from '../containers/AuthButtons';
import { ShareButton } from '../containers/Compartilhar';
import { StyledSection } from '../styled-components/section';

import Logo from '../assets/logo.png';
import backgroundImage from '../assets/layout.png'

const logoStyle = {
    width: '100%',
    maxWidth: 200,
    height: 174.313,
    margin: 'auto',
    borderRadius: 10
}

const HomeScreen = props => {
    return (
        <StyledSection backgroundImage={backgroundImage}>
            <img src={Logo} alt="" style={logoStyle} />
            <HomeNav />
            <div style={{ maxWidth: 320, width: '100%', margin: 'auto' }}>
                <LoginButton />
                <LinkToLoginWithEmailButton />
                <ShareButton>
                    <StyledButton left>
                        <i class="material-icons">
                            share
                    </i>
                        Compartilhar</StyledButton>
                </ShareButton>
            </div>
        </StyledSection>
    )
}

export default connect(user)(HomeScreen);
