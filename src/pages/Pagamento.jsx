import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { order } from '../state/mapStates/map_states';
import { NEW_UPDATE_PAYMENT_METHOD } from '../state/actions/actions';

import { StyledSection, SectionTitle } from '../styled-components/section';
import { StyledButton } from '../styled-components/buttons';

import { Form, Label, Input, Select, FormTitle } from '../styled-components/form';

class Pagamento extends Component {

    state = {
        ready: false,
        metodo: 'cartao',
        troco: 0
    }

    changeHandler = event => {
        const { name, value } = event.target;
        this.setState({ [name]: value })
    }

    submitHandler = event => {
        event.preventDefault();
        this.setState({ ready: true }, _ => this.props.updatePaymentMethod(this.state))

    }

    render() {
        const { ready } = this.state;
        return (
            ready ? <Redirect to='/resumo' />
                : <StyledSection>
                    <SectionTitle>Pagamento</SectionTitle>
                    <Form onSubmit={this.submitHandler}>
                        <FormTitle></FormTitle>
                        <Label htmlFor="select">
                            Forma de pagamento:
                   <Select value={this.state.metodo} onChange={this.changeHandler} name="metodo" valid required>
                                <option value="cartao">Cartão de Crédito</option>
                                <option value="dinheiro">Dinheiro</option>
                            </Select>
                        </Label>
                        {this.state.metodo === 'dinheiro' ?
                            <Label htmlFor="troco">
                                Troco para quanto? (em reais)
                    <Input type="number" value={this.state.trocoPara} onChange={this.changeHandler} name="troco" placeholder="ex. 50" required valid />
                            </Label>
                            : null
                        }
                        <StyledButton type="submit">Concluir</StyledButton>
                    </Form>
                </StyledSection>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updatePaymentMethod: payload => dispatch({ type: NEW_UPDATE_PAYMENT_METHOD, payload })
    }
}

export default connect(order, mapDispatchToProps)(Pagamento);
