import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';

import { actions } from '../state/actions/actions';
import { state } from '../state/mapStates/map_states';

import { StyledSection, SectionTitle, SectionText } from '../styled-components/section';
import { OrderContainer } from '../styled-components/containers';
import { PriceButton } from '../styled-components/buttons';

import accounting from '../utils/formatMoney';

import Counter from '../containers/Counter';
import Parts from '../containers/Parts';
import { ConfirmOrderButtonContainer } from '../styled-components/buttons';

import { PartesButton } from '../styled-components/partes';

import AddToCartButton from '../containers/AddToCartButton';
import RemoveFromCartButton from '../containers/RemoveFromCartButton';

import ProductScreenOptions from '../containers/ProductScreenOptions';

import Observacao from '../containers/Observacao';

class ProductScreen extends Component {
    state = {
        produto: {},
        counter: 1,
        parts: 1
    }

    componentDidMount() {
        const { produto, id } = this.props.match.params;
        const { produtos } = this.props.state.rootReducer;

        if (produtos[produto].length === 0) return <Redirect to='/404' />;
        const item = produtos[produto].filter(item => item.id.toLowerCase() === id);

        this.setState({ produto: item[0] });
        this.props.resetCartReducer()
        this.props.changeProduct(item[0]);
    }

    render() {
        const { produto } = this.state;
        const { parts } = this.props.state.cartReducer.cart
        const routerParams = this.props.match.params;
        return (
            produto.name ?
                <StyledSection>
                    <SectionTitle>
                        {produto.name}
                        <PriceButton>{accounting.formatMoney(produto.value)}</PriceButton>
                    </SectionTitle>
                    <SectionText>{produto.ingredients}</SectionText>
                    <OrderContainer>
                        <Counter />
                        <Parts />
                        {(routerParams.produto === 'pizzas')
                            && parts <= 1 ? <ProductScreenOptions />
                            : null}
                        {parts > 1
                            ? <Link to='/partes'>
                                <PartesButton filled="true" full >
                                    Escolher partes
                                </PartesButton>
                            </Link>
                            : null}
                        {parts <= 1 ? <Observacao /> : null}
                    </OrderContainer>
                    {parts <= 1
                        ? <ConfirmOrderButtonContainer>
                            <RemoveFromCartButton />
                            <AddToCartButton />
                        </ConfirmOrderButtonContainer>
                        : null}
                </StyledSection> :
                null
        )
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        newOrder: payload => dispatch({ type: actions.NEW_ORDER, payload }),
        removeOrder: payload => dispatch({ type: actions.NEW_REMOVE_ORDER, payload }),
        changeProduct: payload => dispatch({ type: actions.UPDATE_CHANGE_PRODUCT, payload }),
        resetCartReducer: _ => dispatch({ type: actions.NEW_RESET_CART_REDUCER }),
        addCurrentCartOptions: payload => dispatch({ type: actions.NEW_CURRENT_CART_OPTIONS, payload })
    }
}

export default connect(state, mapDispatchToProps)(ProductScreen);
