import React, { Component } from 'react';
import { connect } from 'react-redux';

import { StyledSection } from '../styled-components/section';

import UserProfile from '../containers/UserProfile';
import UserContact from '../containers/UserContact';
import UserPedidosCounter from '../containers/UserPedidosCounter';

import { LoginButton, LinkToLoginWithEmailButton, LogoutButton, DeleteAccountButton } from '../containers/AuthButtons'

class UserScreen extends Component {
    render() {
        return (
            <StyledSection>
                <UserProfile />
                <UserPedidosCounter />
                <UserContact />
                <LoginButton />
                <LinkToLoginWithEmailButton />
                <LogoutButton />
                <DeleteAccountButton />
            </StyledSection>
        )
    }
}

export default connect()(UserScreen);