import React from 'react';

import { StyledSection, SectionTitle } from '../styled-components/section';
import { InformacoesSection, InformacoesContainer, InformacoesTitle, InformacoesSubtitle, InformacoesLink } from '../styled-components/informacoes'

const InformacoesScreen = _ => (
    <StyledSection>
        <SectionTitle>Informações</SectionTitle>
        <InformacoesSection>
            <InformacoesContainer>
                <InformacoesTitle>Horário de atendimento</InformacoesTitle>
                <InformacoesSubtitle>
                    De Segunda-feira a Domingo, das 18:00 as 00:00
            </InformacoesSubtitle>
            </InformacoesContainer>
            <InformacoesContainer>
                <InformacoesTitle>Endereço</InformacoesTitle>
                <InformacoesSubtitle>
                    Bairro das Flores, Rua São José, n° 152, Porto Velho, Ro.
                </InformacoesSubtitle>
            </InformacoesContainer>
            <InformacoesContainer>
                <InformacoesTitle>Telefones</InformacoesTitle>
                <InformacoesTitle small>Telefone 1</InformacoesTitle>
                <InformacoesSubtitle>
                    (11) 9 5555-5555
                </InformacoesSubtitle>
                <InformacoesTitle small>Telefone 2</InformacoesTitle>
                <InformacoesSubtitle>
                    (11) 9 4444-5555
                </InformacoesSubtitle>
            </InformacoesContainer>
            <InformacoesContainer>
                <InformacoesTitle>Web</InformacoesTitle>
                <InformacoesTitle small>Facebook</InformacoesTitle>
                <InformacoesSubtitle>
                    <InformacoesLink href='https://facebook.com'>Saborosa Pizza</InformacoesLink>
                </InformacoesSubtitle>
                <InformacoesTitle small>Instagram</InformacoesTitle>
                <InformacoesSubtitle>
                    <InformacoesLink href='https://instagram.com'>@saborosa_pizza</InformacoesLink>
                </InformacoesSubtitle>
            </InformacoesContainer>
        </InformacoesSection>
    </StyledSection>
);

export default InformacoesScreen;

