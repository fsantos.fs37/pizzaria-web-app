import React from 'react';
import { StyledSection } from '../styled-components/section';

const styles = {
    margin: 'auto',
    fontFamily: 'Arial, sans-serif',
    color: '#ffffff'
}

const LoadingPage = props => {
    return (
        <StyledSection>
            <div style={styles}>
                <h1>Carregando...</h1>
            </div>
        </StyledSection>
    )
}

export default LoadingPage;
