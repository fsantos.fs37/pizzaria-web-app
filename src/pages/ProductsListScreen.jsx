import React, { Component } from 'react';
import { connect } from 'react-redux';

import { pizzas } from '../state/mapStates/map_states';

import { StyledSection, SectionTitle } from '../styled-components/section';
import { SearchForm, SearchInput, SearchInputRadio, SearchLabel } from '../styled-components/search';
import SearchResults from '../containers/SearchResults';

import Banner from "../containers/Banner";

class ProductsListScreen extends Component {

    state = {
        searchString: '',
        orderBy: {
            preco: false,
            alfa: true,
        }
    }

    changeHandler = event => {
        const { name, value } = event.target;
        switch (name) {
            case 'search':
                this.setState({ searchString: value });
                break
            case 'preco':
                this.setState({ orderBy: { preco: !this.state.orderBy.preco, alfa: false } });
                break
            case 'alfa':
                this.setState({ orderBy: { alfa: !this.state.alfa, preco: false } });
                break
            default:
                return;
        }
    }

    render() {
        // As informações do nome do produto, o tipo e o tamanho 
        // são extraidas dos parâmetros de url através do objet "match"
        const { produto, tamanho, tipo } = this.props.match.params;
        const title = `${produto} ${tamanho ? tamanho === 'unset' ? '' : tamanho + 's' : ''}`
        console.log(tipo);

        return (
            <StyledSection>
                {tipo === 'pizzasCombo' && <Banner />}
                <SearchForm onSubmit={e => e.preventDefault()}>
                    <SearchInput placeholder={`Pesquisar em ${title}`} type="text" value={this.state.searchString} onChange={this.changeHandler} name='search' />
                    Ordernar por:
                    <div style={{ display: 'flex' }}>
                        <SearchLabel>
                            <SearchInputRadio type='radio' name='preco' checked={this.state.orderBy.preco} onClick={this.changeHandler} onChange={this.changeHandler} /> Menor preço
                        </SearchLabel>

                        <SearchLabel>
                            <SearchInputRadio type='radio' name='alfa' checked={this.state.orderBy.alfa} onClick={this.changeHandler} onChange={this.changeHandler} /> Ordem alfabética
                        </SearchLabel>

                    </div>
                </SearchForm>
                <SectionTitle>{title}</SectionTitle>

                {
                    // Se o usuário começa a escrever, o componente filtra os resultados da busca e
                    // rendereiza a seção de resultados da busca ao invés da lista de produtos desta categoria
                    // this.state.searchString.length > 0
                    <SearchResults
                        name={this.state.searchString}
                        produto={produto}
                        tipo={tipo}
                        tamanho={tamanho}
                        orderBy={this.state.orderBy}
                    />
                }
            </StyledSection>
        )
    }
};

export default connect(pizzas)(ProductsListScreen);
