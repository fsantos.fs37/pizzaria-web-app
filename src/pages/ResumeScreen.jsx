import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { database, auth } from '../firebase/firebase';

import { order, price, adress, payment, user } from '../state/mapStates/map_states';
import { NEW_ORDER_SUMITTED, NEW_RESET_CART_REDUCER, NEW_RECEIVED_ID, UPDATE_STAGE_ORDER } from '../state/actions/actions';

import { StyledSection, SectionTitle } from '../styled-components/section';
import { FinalizarPedidoButton } from '../styled-components/pedido';
import {
    AdressCardContainer,
    AdressCardTitle,
    AdressCardText
} from '../styled-components/adressCard';

import { PriceTag } from '../styled-components/buttons';
import { PedidoCard } from '../containers/PedidoCard';

import accounting from '../utils/formatMoney';

class ResumeScreen extends Component {

    state = {
        isPromo: false,
        orderSumited: false,
        pedido: {}
    }

    componentDidMount() {
        let { order: { order }, price: { price }, adress: { adress }, payment: { payment } } = this.props;

        const filterOrderPromo = order.filter(item => item.tipo === 'promocao');

        if (filterOrderPromo.length >= 1) {
            console.log(filterOrderPromo)
            this.setState({ isPromo: true })
        }

        delete adress.ready
        delete payment.ready

        //Remove propriedades desnecessárias
        for (let prop in adress) {
            delete adress[prop].required;
            delete adress[prop].valid;
        }

        const pedido = { order, price, adress, payment, status: false };

        this.setState({ pedido });
    }

    splitPoint = _ => {
        const { uid } = auth.currentUser;

        const userRef = database.collection('users').doc(uid);

        database.runTransaction(transaction => {
            return transaction.get(userRef).then(res => {
                let pedidosArray = res.data().pedidosArray;
                pedidosArray = pedidosArray
                    .map((item, i) => i > 10 ? item : null).filter(Boolean);

                transaction.update(userRef, { pedidosArray });
            })
            // userRef.get().then(res => {
            //     console.log(res.data());
            //     let pedidosArray = res.data().pedidosArray;
            //     pedidosArray = pedidosArray.slice(0, 25);
            //     })
        })
    }

    submitOrder = event => {

        const req1 = () => window.confirm('Confirmar pedido?');
        const req2 = () => window.confirm('Realizar pedido, você gastará o total necessário de pontos dessa promoção!');
        let req3 = null;
        let req4 = null;

        if (!this.state.isPromo) {
            req4 = req1();
        }

        if (req4 === false) {
            return
        }

        if (this.state.isPromo) {
            req3 = req2();
        }

        if (req3 === false) {
            return
        }

        if (req3 === true) {
            this.splitPoint()
        }

        if (this.state.pedido.order.length < 1) return;

        event.target.disabled = true;

        const { user } = this.props.user;

        database.collection('pedidos').add(this.state.pedido).then(res => {
            alert('Pedido enviado, aguarde a confirmação!');
            this.setState({ orderSumited: true });
            this.props.orderSubmitted(this.state.pedido);
            this.props.receivedOrderId(res.id);
            this.props.newStagedOrder();
            this.props.resetCart();
        }).then(_ => {
            if (user.uid) {
                const userRef = database.collection('users').doc(user.uid);
                database.runTransaction(transaction => {
                    return transaction.get(userRef)
                        .then(userDoc => {
                            if (!userDoc.exists) {
                                return;
                            };
                            const userData = userDoc.data();
                            if (userData.pedidosArray) {
                                userData.pedidosArray.push(this.state.pedido)
                            }
                            else {
                                userData.pedidosArray = [this.state.pedido];
                            }
                            transaction.update(userRef, userData);
                        })
                })

            }
        })
    }

    render() {
        const { orderSumited } = this.state;
        const { order: { order }, price: { price }, adress: { adress } } = this.props;
        const orders = order.map(order => {
            return (
                <PedidoCard
                    key={order.id}
                    type={order.type}
                    title={order.name}
                    counter={order.counter}
                    value={order.value}
                    totalValue={order.totalValue}
                    produto={order.type}
                    id={order.id}
                    adicionais={order.adicionais}
                    partes={order.partsOptions}
                    borda={order.bordas}
                    observacao={order.observacao}
                />
            )
        })

        return (
            orderSumited
                ? <Redirect to='/pedidos' />
                : <StyledSection>
                    <SectionTitle>Resumo</SectionTitle>
                    {orders}
                    {price
                        ? <FinalizarPedidoButton filled>
                            Total
                        <PriceTag>
                                {accounting.formatMoney(price)}
                            </PriceTag>
                        </FinalizarPedidoButton> : null}
                    {adress.rua ? <AdressCardContainer>
                        <AdressCardTitle>Endereço para entrega</AdressCardTitle>
                        <AdressCardText>Rua {adress.rua.value}</AdressCardText>
                        <AdressCardText>N° {adress.numero.value}</AdressCardText>
                        <AdressCardText>Bairro {adress.bairro.value}</AdressCardText>
                        <AdressCardText>CEP {adress.cep.value}</AdressCardText>
                        <AdressCardText>Complemento {adress.complemento.value}</AdressCardText>
                    </AdressCardContainer> : null}
                    <FinalizarPedidoButton onClick={this.submitOrder} filled center>Finalizar pedido.</FinalizarPedidoButton>
                </StyledSection>
        )
    }
}

const mapStateToProps = state => {
    return {
        order: order(state),
        price: price(state),
        adress: adress(state),
        payment: payment(state),
        user: user(state),
    }
}

const mapDistpatchToProps = dispatch => {
    return {
        orderSubmitted: payload => dispatch({ type: NEW_ORDER_SUMITTED, payload }),
        resetCart: _ => dispatch({ type: NEW_RESET_CART_REDUCER }),
        receivedOrderId: payload => dispatch({ type: NEW_RECEIVED_ID, payload }),
        newStagedOrder: _ => dispatch({ type: UPDATE_STAGE_ORDER }),
    }
}

export default connect(mapStateToProps, mapDistpatchToProps)(ResumeScreen);
