import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { database } from '../firebase/firebase';

import { orderReducer, price, adress, orderId, orderStatus } from '../state/mapStates/map_states';
import { NEW_STATUS_UPDATE, NEW_RESET_CART_REDUCER, RESET_CART_REDUCER } from '../state/actions/actions';

import { StyledSection, SectionTitle } from '../styled-components/section';
import { FinalizarPedidoButton, EmptyOrderMessage, StatusButton, StatusTag } from '../styled-components/pedido';
import { PriceTag } from '../styled-components/buttons';

import { PedidoCard } from '../containers/PedidoCard';

import accounting from '../utils/formatMoney';

class PedidoScreen extends Component {

    state = {
        orderId: '',
        status: false,
        orderStatus: 'A confirmar'
    }

    constructor(props) {
        super(props);
        this.watchOrderStatus = this.watchOrderStatus.bind(this);
    }

    componentDidMount() {
        this.props.resetCart()
    }

    watchOrderStatus() {
        const { orderId } = this.props.orderId;
        if (!orderId) return;
        database.collection('pedidos').doc(orderId).onSnapshot(snapShot => {
            const data = snapShot.data();
            if (data.status && !this.state.status) {
                this.setState({ status: true, orderStatus: data.status })
            }
        })
    }

    render() {
        this.watchOrderStatus()
        const { orderReducer } = this.props.orderReducer;
        const pedido = orderReducer.order;
        const { order, price } = pedido;

        const orders = pedido.order ? order.map(order => {
            return (
                <PedidoCard
                    disabledLink
                    key={order.id}
                    type={order.type}
                    title={order.name}
                    counter={order.counter}
                    value={order.value}
                    totalValue={order.totalValue}
                    produto={order.type}
                    id={order.id}
                    adicionais={order.adicionais}
                    partes={order.partsOptions}
                    borda={order.bordas}
                />
            )
        }) : []

        return (
            <StyledSection>
                <SectionTitle>Pedidos</SectionTitle>
                {pedido.order
                    ? <StatusButton filled>
                        Status
                    <StatusTag>
                            {this.state.status === true ? 'Confirmado' : 'Aguarde confirmação'}
                        </StatusTag>
                    </StatusButton>
                    : null
                }
                {orders}
                {price
                    ? <FinalizarPedidoButton filled>
                        Total
                        <PriceTag>
                            {accounting.formatMoney(price)}
                        </PriceTag>
                    </FinalizarPedidoButton> : null}
                {orders.length === 0
                    ? [
                        <EmptyOrderMessage key='order-empty-message'>Você ainda não possui pedidos</EmptyOrderMessage>,
                        <Link to='/menu' key='no-orders-button'>
                            <FinalizarPedidoButton center filled>Ver o menu</FinalizarPedidoButton>
                        </Link>
                    ]
                    : null

                }
            </StyledSection>
        )
    }
}

const mapStateToProps = state => {
    return {
        orderReducer: orderReducer(state),
        price: price(state),
        adress: adress(state),
        orderId: orderId(state),
        orderStatus: orderStatus(state),
    }
}

const mapDispatchToProps = dispatch => {
    return {
        newStatusUpdate: payload => dispatch({ type: NEW_STATUS_UPDATE, payload }),
        resetCart: _ => dispatch({ type: RESET_CART_REDUCER })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PedidoScreen);
