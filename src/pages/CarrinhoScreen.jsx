import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { order, price, adress } from '../state/mapStates/map_states';

import { StyledSection, SectionTitle } from '../styled-components/section';
import { FinalizarPedidoButton, EmptyOrderMessage } from '../styled-components/pedido';
import { PriceTag } from '../styled-components/buttons';

import { PedidoCard } from '../containers/PedidoCard';

import accounting from '../utils/formatMoney';

class PedidoScreen extends Component {
    render() {
        const { order } = this.props.order;
        const { price } = this.props.price;
        const { adress } = this.props.adress;
        const orders = order.map(order => {
            return (
                <PedidoCard
                    key={order.id}
                    type={order.type}
                    title={order.name}
                    counter={order.counter}
                    value={order.value}
                    totalValue={order.totalValue}
                    produto={order.type}
                    id={order.id}
                    adicionais={order.adicionais}
                    partes={order.partsOptions}
                    borda={order.bordas}
                    observacao={order.observacao}
                />
            )
        })
        return (
            <StyledSection>
                <SectionTitle>Carrinho</SectionTitle>
                {orders}
                {price
                    ? <FinalizarPedidoButton filled>
                        Total
                        <PriceTag>
                            {accounting.formatMoney(price)}
                        </PriceTag>
                    </FinalizarPedidoButton> : null}
                {orders.length > 0
                    ? <Link to={adress.rua ? '/pagamento' : '/confirmar'}>
                        <FinalizarPedidoButton center filled>Próximo</FinalizarPedidoButton>
                    </Link>
                    : [
                        <EmptyOrderMessage key='order-empty-message'>Você ainda não possui pedidos</EmptyOrderMessage>,
                        <Link to='/menu' key='no-orders-button'>
                            <FinalizarPedidoButton center filled>Ver o menu</FinalizarPedidoButton>
                        </Link>]

                }
            </StyledSection>
        )
    }
}

const mapStateToProps = state => {
    return {
        order: order(state),
        price: price(state),
        adress: adress(state)
    }
}

export default connect(mapStateToProps)(PedidoScreen);
