import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import { price, cart, order } from '../state/mapStates/map_states';

import accounting from '../utils/formatMoney';

import { StyledFooter, InnerFooter, FooterPriceButton, FooterOrderButton } from '../styled-components/footer';

const FooterContainer = props => {
    console.log(props)
    const { order } = props.order;
    const { cart } = props.cart;
    const { price } = props.price;
    const hry = window.location.pathname === '/pedidos'
        || window.location.pathname === '/confirmar'
        || window.location.pathname === '/admin'
        || window.location.pathname === '/resumo'
        || window.location.pathname === '/carrinho';
    return (
        <StyledFooter visible={(price > 0 && !hry) || order.length > 0 && cart.produto.tipo === 'promocao'}>
            <InnerFooter>
                <FooterPriceButton >{accounting.formatMoney(price)}</FooterPriceButton>
                <Link to='/carrinho'>
                    <FooterOrderButton>Ver carrinho</FooterOrderButton>
                </Link>
            </InnerFooter>
        </StyledFooter>
    )
}

const mapStateToProps = state => {
    return {
        cart: cart(state),
        price: price(state),
        order: order(state),
    }
}

export default withRouter(connect(mapStateToProps)(FooterContainer));
