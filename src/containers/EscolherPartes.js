import React, { Component } from 'react';
import { connect } from 'react-redux';

import { order, cart, produtos } from '../state/mapStates/map_states';

import Accordion from './Accordion';

import accounting from '../utils/formatMoney';
import {
    PartOptionContainer,
    PartOptionInput,
    PartOptionLabel,
} from '../styled-components/partes';


class EscolherPartes extends Component {
    state = {
        marked: []
    }

    constructor(props) {
        super(props)
        this.changeHandler = this.changeHandler.bind(this);
        this.clickHandler = this.clickHandler.bind(this);
    }

    clickHandler(e) {
        const markedList = this.state.marked;
        markedList.push(e.target.firstElementChild.value)
        this.setState()
    }

    changeHandler() {

    }

    render() {
        return (
            <div>
                <PartOptionContainer>
                    <PartOptionLabel
                        htmlFor={'chandler'}
                        onClick={this.clickHandler}
                        checked={this.state.marked.includes('chandler')}>
                        ola
                        <PartOptionInput name="chandler" value="chandler" onChange={this.changeHandler} />
                    </PartOptionLabel>
                </PartOptionContainer>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        produtos: state => produtos(state),
        cart: state => cart(state)
    }
}

export default connect(mapStateToProps)(EscolherPartes)