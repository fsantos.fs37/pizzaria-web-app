import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import { bordasCart, bordas } from '../state/mapStates/map_states'
import { actions } from '../state/actions/actions'

import { AdicionaisLabel, AdicionaisPrice, AdicionaisInput } from '../styled-components/adicionais';


import accounting from '../utils/formatMoney';

class BordasOptions extends Component {
    state = {
        value: ''
    }

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.UpdateBordasReducer = this.UpdateBordasReducer.bind(this);
    }

    componentDidMount() {
    }

    handleClick(e) {
        const value = e.currentTarget.querySelector('input').value;
        if (this.state.value === value) {
            return this.setState({ value: '' }, () => this.UpdateBordasReducer());
        }
        this.setState({ value }, () => this.UpdateBordasReducer())
    }

    UpdateBordasReducer() {
        const { bordas } = this.props.bordas;
        let borda = bordas.filter(item => item.id === this.state.value)[0];

        if (borda) {
            this.props.updateBordas(borda)
        }
        else {
            this.props.updateBordas({})

        }
    }

    handleChange(e) {
    }

    render() {
        const { bordas } = this.props.bordas;
        const options = bordas.map((item, index) => (
            <AdicionaisLabel
                htmlFor={item.name}
                value={index + 1}
                onClick={this.handleClick}
                marked={this.state.value.includes(item.id)}
                filled="true"
                full
                key={item.name}>
                <div>{item.name}</div>
                <AdicionaisPrice>{accounting.formatMoney(item.value)}</AdicionaisPrice>
                <AdicionaisInput name={item.name} type="radio" value={item.id} onChange={this.handleChange} />
            </AdicionaisLabel>)
        )
        return (
            <Fragment>
                {options}
            </Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateBordas: payload => dispatch({ type: actions.NEW_UPDATE_BORDAS_OPTIONS, payload })
    }
}

const mapStateToProps = state => {
    return {
        bordasCart: bordasCart(state),
        bordas: bordas(state)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BordasOptions);
