import React, { Component } from 'react';
import { connect } from 'react-redux';
import { database } from '../firebase/firebase';

import { BannerImg } from '../styled-components/Banner';

class Banner extends Component {
    state = {
        imageURL: null
    }

    componentDidMount() {
        database.collection('promocoes').doc('banner').get()
            .then(doc => {
                this.setState({ imageURL: doc.data().bannerURL })
            })
    }

    render() {
        return (
            <BannerImg src={this.state.imageURL} />
        )
    }
}

export default connect()(Banner);
