import React, { Component } from 'react';
import { connect } from 'react-redux';

import { user } from '../state/mapStates/authState';

import { ProfileContainer, Avatar, UserName } from '../styled-components/userProfile';

class UserProfile extends Component {
    render() {
        const { user } = this.props;
        return (
            <ProfileContainer>
                {user.photoURL ? <Avatar src={user.photoURL} /> : null}
                {user.displayName ? <UserName>{user.displayName}</UserName> : null}
                {user.email ? <UserName>{user.email}</UserName> : null}
            </ProfileContainer>
        )
    }
}

export default connect(user)(UserProfile);
