import React from 'react';
import { Link } from 'react-router-dom';

import { StyledHeaderButton } from '../styled-components/buttons';

export const BackButton = props => {
    const visibility = window.location.pathname === '/';
    const hry = window.history.state ? window.history.state.key ? true : false : null;
    return (
        <StyledHeaderButton invisible={visibility} onClick={_ => {
            hry ? window.history.back() : window.location.pathname = '/';
        }}>
            <i className="material-icons">arrow_back_ios</i>
        </StyledHeaderButton>
    )
}

export const HomeScreenButton = props => {
    const visibility = window.location.pathname === '/';
    // const hry = window.history.state ? window.history.state.key ? true : false : null;
    return (
        <Link to='/'>
            <StyledHeaderButton large invisible={visibility}>
                Início
                {/* <i className="material-icons">home</i> */}
            </StyledHeaderButton>
        </Link>
    )
}
