import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import { pizzas, cart, adicionais, adicionaisCart, order } from '../state/mapStates/map_states'
import { actions } from '../state/actions/actions'

import { PartsLabel, PartsIngredients, PartsInput } from '../styled-components/partsButtons';

import Accordion from '../containers/Accordion';
import { AccordionButton, AccordionBody, AccordionPane } from '../styled-components/accordion';

class EscPartes extends Component {
    state = {
        limit: 0,
        value: 1,
        values: []
    }

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.updatePartsOptionsReducer = this.updatePartsOptionsReducer.bind(this);
    }

    componentDidMount() {
        const { cart } = this.props.cart;
        this.setState({ limit: cart.parts });
    }

    addToCart() {
        this.props.newOrder(this.formatOrder());
    }

    handleClick(e) {
        const value = e.currentTarget.querySelector('input').value;
        const values = [...new Set(this.state.values)];
        if (values.includes(value)) {
            const filteredValues = values.filter(item => item !== value);
            this.setState({
                values: filteredValues
            }, () => this.updatePartsOptionsReducer()
            )
            return console.log('exists')
        }
        if (this.state.values.length === this.state.limit) return;
        values.push(value)
        this.setState({
            values
        }, () => this.updatePartsOptionsReducer());

    }

    formatOrder() {
        const { produto, counter, adicionais, parts, partsOptions } = this.props.cart.cart;
        const comAdicionais = adicionais.map(item => item.value).reduce((prev, next) => prev + next, 0)
        const order = {
            ...produto,
            value: produto.value * counter + comAdicionais,
            counter,
            adicionais,
            parts,
            partsOptions
        };
        return order;
    }

    updatePartsOptionsReducer() {
        const { pizzas } = this.props.pizzas;
        const adicionaisObject = this.state.values.map(id => {
            return pizzas.filter(item => item.id === id)[0]
        });
        this.props.updateParts(adicionaisObject);
        console.log(this.props)
    }

    handleChange(e) {
    }

    render() {
        const { pizzas } = this.props.pizzas;
        const options = pizzas.filter(item => item.tipo !== 'pizzasCombo').map((item, index) => (
            <AccordionPane key={item.name + index}>
                <PartsLabel
                    htmlFor={item.name}
                    value={index + 1}
                    onClick={this.handleClick}
                    marked={this.state.values.includes(item.id)}
                    filled="true"
                    full
                >
                    <div>{item.name}</div>
                    <PartsIngredients>{item.ingredients}</PartsIngredients>
                    <PartsInput name={item.name} type="radio" value={item.id} onChange={this.handleChange} />
                </PartsLabel>
            </AccordionPane>
        )
        )
        return (
            <Fragment>{options}</Fragment>

        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateParts: payload => dispatch({ type: actions.NEW_UPDATE_PARTS_OPTIONS, payload }),
        newOrder: payload => dispatch({ type: actions.UPDATE_ORDER, payload })
    }
}

const mapStateToProps = state => {
    return {
        adicionais: adicionais(state),
        adicionaisCart: adicionaisCart(state),
        pizzas: pizzas(state),
        cart: cart(state),
        order: order(state),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EscPartes);
