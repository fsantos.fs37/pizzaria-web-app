import React, { Component } from 'react';
import { connect } from 'react-redux';

import { actions } from '../state/actions/actions';
import { counter, parts } from '../state/mapStates/map_states';

import { OrderInfoContainer, OrderInfoTitle } from '../styled-components/orderInfo';
import { CounterButtonContainer, CounterButton, CounterValue } from '../styled-components/counter';

class Counter extends Component {
    constructor(props) {
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler(event) {
        if (event.target.name === 'counter_value' && event.target.value >= 1) {
            this.props.changeCounter(parseInt(event.target.value, 10));
        }
    }

    render() {
        const { increment, decrement, counter: { counter }, parts: { parts } } = this.props;
        return (
            parts > 1
                ? null
                : <OrderInfoContainer>
                    <OrderInfoTitle>Quantidade:</OrderInfoTitle>
                    <CounterButtonContainer>
                        <CounterButton onClick={decrement}>
                            <i className="material-icons">remove</i>
                        </CounterButton>
                        <CounterValue name='counter_value' type='number' value={parseInt(counter, 10)} onChange={this.changeHandler} />
                        <CounterButton onClick={increment}>
                            <i className="material-icons">add</i>
                        </CounterButton>
                    </CounterButtonContainer>
                </OrderInfoContainer>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        increment: _ => dispatch({ type: actions.UPDATE_INCREMENT_COUNTER }),
        decrement: _ => dispatch({ type: actions.UPDATE_DECREMENT_COUNTER }),
        changeCounter: payload => dispatch({ type: actions.UPDATE_CHANGE_COUNTER, payload }),
    }
}

const mapStateToProps = state => {
    return {
        counter: counter(state),
        parts: parts(state),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
