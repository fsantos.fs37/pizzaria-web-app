import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import { adicionais, adicionaisCart } from '../state/mapStates/map_states'
import { actions } from '../state/actions/actions'

import { AdicionaisLabel, AdicionaisPrice, AdicionaisInput } from '../styled-components/adicionais';

import accounting from '../utils/formatMoney';

class AdicionaisOptions extends Component {

    state = {
        value: 1,
        values: []
    }

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.updateAdicionaisReducer = this.updateAdicionaisReducer.bind(this);
    }

    componentDidMount() {
        const { adicionaisCart } = this.props.adicionaisCart;
        const values = adicionaisCart.map(item => item.id)
        this.setState({ values })
    }

    handleClick(e) {
        const value = e.currentTarget.querySelector('input').value;
        const values = [...new Set(this.state.values)];
        if (values.includes(value)) {
            const filteredValues = values.filter(item => item !== value);
            this.setState({
                values: filteredValues
            }, () => this.updateAdicionaisReducer()
            )
            return;
        }
        values.push(value)
        this.setState({
            values
        }, () => this.updateAdicionaisReducer())
    }

    updateAdicionaisReducer() {
        const { adicionais } = this.props.adicionais;
        const adicionaisObject = this.state.values.map(id => {
            return adicionais.filter(item => item.id === id)[0]
        })
        this.props.updateAdicionais(adicionaisObject)
    }

    handleChange(e) {
    }

    render() {
        const { adicionais } = this.props.adicionais;
        const options = adicionais.map((item, index) => (
            <AdicionaisLabel
                htmlFor={item.name}
                value={index + 1}
                onClick={this.handleClick}
                marked={this.state.values.includes(item.id)}
                filled="true"
                full
                key={item.name}>
                <div>{item.name}</div>
                <AdicionaisPrice>{accounting.formatMoney(item.value)}</AdicionaisPrice>
                <AdicionaisInput name={item.name} type="radio" value={item.id} onChange={this.handleChange} />
            </AdicionaisLabel>)
        )
        return (
            <Fragment>{options}</Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateAdicionais: payload => dispatch({ type: actions.UPDATE_CHANGE_ADICIONAIS, payload })
    }
}

const mapStateToProps = state => {
    return {
        adicionais: adicionais(state),
        adicionaisCart: adicionaisCart(state)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdicionaisOptions);
