import React from 'react';
import { Link } from 'react-router-dom';

import { HomeNavContainer, SquaredButton } from '../styled-components/homeNav';
import { ShareButton } from './Compartilhar';

const HomeNav = _ => {
    return (
        <HomeNavContainer>
            <Link to='/menu'>
                <SquaredButton>
                    <i className="material-icons icon">restaurant_menu</i>
                    Ver menu
                        </SquaredButton>
            </Link>
            <Link to='/promocoes'>
                <SquaredButton>
                    <i className="material-icons icon">card_giftcard</i>
                    Junte e ganhe
            </SquaredButton>

            </Link>
            <Link to='/pedidos'>
                <SquaredButton>
                    <i className="material-icons icon">local_pizza</i>
                    Meus pedidos
                        </SquaredButton>
            </Link>
            <Link to='/produto/pizzas/unset/pizzasCombo'>
                <SquaredButton>
                    <i className="material-icons icon">star</i>
                    Promoções
                </SquaredButton>
            </Link>
            <Link to='/conta'>
                <SquaredButton>
                    <i className="material-icons icon">account_circle</i>
                    Minha conta
                    </SquaredButton>
            </Link>
            <Link to='/informacoes'>
                <SquaredButton>
                    <i className="material-icons icon">info</i>
                    Informações
                </SquaredButton>
            </Link>
        </HomeNavContainer>
    )
}


export default HomeNav;
