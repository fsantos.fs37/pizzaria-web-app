import React, { Component } from 'react';
import { connect } from 'react-redux';

import { orderBy as _orderBy } from 'lodash';

import { produtos } from '../state/mapStates/map_states'

import { MenuContainer } from '../styled-components/menu';
import ProductButton from './ProductButton';
import PizzaComboButton from './PizzaComboButton';

class SearchResults extends Component {

    render() {
        const { produto, produtos, tipo, tamanho, name, orderBy } = this.props;
        const condition = item => {
            return item.tipo === tipo
                && (
                    item.size === tamanho
                    || item.size === ''
                    || item.size === 'unset'
                    || !item.size
                )
                ? item.ingredients ? item.ingredients.toLowerCase().includes(name.toLowerCase()) : null || item.name.toLowerCase().includes(name.toLowerCase()) : null
        }
        let filteredElements = produtos[produto]


        if (tipo !== 'promocao') {
            filteredElements = produtos[produto].filter(condition)
        }

        if (orderBy.preco) {
            filteredElements = _orderBy(filteredElements, ['value'], ['asc'])
        }

        if (orderBy.alfa) {
            filteredElements = _orderBy(filteredElements, ['name'], ['asc'])
        }

        if (tipo === 'pizzasCombo') {
            filteredElements = filteredElements.map(p => <PizzaComboButton key={p.id} data={p} produto={produto} />);
        }
        else {
            filteredElements = filteredElements.map(p => <ProductButton key={p.id} data={p} produto={produto} />);
        }


        return (
            <MenuContainer>
                {filteredElements}
            </MenuContainer >
        )
    }
}

export default connect(produtos)(SearchResults);
