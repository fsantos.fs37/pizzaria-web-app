import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { user } from '../state/mapStates/authState';

import { signIn, signOut, createWithEmail, deleteAccount } from '../firebase/firebase';

import { StyledButton } from '../styled-components/buttons';

import googleIcon from "../assets/googleIcon.svg";

export const LoginButton = connect(user)(props => {
    return (
        !props.user.uid
            ? <StyledButton left onClick={signIn}>
                <img className='button-icon' src={googleIcon} />
                Entrar com Google</StyledButton>
            : null
    )
})

export const CreateWithEmailButton = connect(user)(props => {
    return (
        !props.user.uid
            ? <StyledButton onClick={_ => {
                props.form.name && props.form.email && props.form.password
                    ? createWithEmail(props.user) : null
            }}>Criar conta</StyledButton>
            : null
    )
})

export const LinkToLoginWithEmailButton = connect(user)(props => {
    return (
        !props.user.uid
            ?
            <Link to='/login'>
                <StyledButton left>
                    <i className="material-icons">email</i>
                    Entrar com email e senha</StyledButton>
            </Link> : null
    )
})

export const LogoutButton = connect(user)(props => {
    return (
        props.user.uid
            ? <StyledButton onClick={signOut} unFilled white>Sair</StyledButton>
            : null
    )
})

export const DeleteAccountButton = connect(user)(props => {
    return (
        props.user.uid
            ? <StyledButton onClick={deleteAccount} danger unFilled>
                Apagar minha conta</StyledButton>
            : null
    )
})

