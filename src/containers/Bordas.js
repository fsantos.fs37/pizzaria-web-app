import React, { Component } from 'react';
import { connect } from 'react-redux';

import { bordas, bordasCart } from '../state/mapStates/map_states'

import { AccordionButton, AccordionBody, AccordionPane } from '../styled-components/accordion';
import { AdicionaisButton } from '../styled-components/adicionais';

import Accordion from './Accordion';

import BordasOptions from './BordasOptions';

class Bordas extends Component {
    render() {
        return (
            <Accordion>
                <AccordionPane className='accordion-child' >
                    <AccordionButton >
                        <AdicionaisButton center filled full >Bordas</AdicionaisButton>
                    </AccordionButton>
                    <AccordionBody >
                        <BordasOptions />
                    </AccordionBody>
                </AccordionPane>
            </Accordion>
        )
    }
}

const mapStateToProps = state => {
    return {
        bordas: bordas(state),
        bordasCart: bordasCart(state)
    }
}

export default connect(mapStateToProps)(Bordas);
