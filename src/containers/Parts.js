import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { actions } from '../state/actions/actions';
import { parts } from '../state/mapStates/map_states';

import { OrderInfoContainer, OrderInfoTitle, OrderListContainer } from '../styled-components/orderInfo';

import PartsOptions from '../containers/PartsOptions';

class Parts extends Component {
    render() {
        const { match } = this.props;
        return (
            match.params.produto === 'pizzas'
                ? <OrderInfoContainer>
                    <OrderInfoTitle>Partes:</OrderInfoTitle>
                    <OrderListContainer>
                        <PartsOptions />
                    </OrderListContainer>
                </OrderInfoContainer>
                : null
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeParts: payload => dispatch({ type: actions.UPDATE_CHANGE_PARTS, payload }),
    }
}

export default connect(parts, mapDispatchToProps)(withRouter(Parts));
