import React, { Component } from 'react';

import { StyledHeaderContainer, StyledHeader } from '../styled-components/header';
import { HomeScreenButton, BackButton } from './HeaderButtons';

class Header extends Component {
    render() {
        const hry = window.location.pathname === '/' || window.location.pathname === '/admin'
            ;
        return (
            <StyledHeaderContainer visible={!hry}>
                <StyledHeader>
                    <BackButton />
                    <HomeScreenButton />
                </StyledHeader>
            </StyledHeaderContainer>
        );
    }
}

export default Header;
