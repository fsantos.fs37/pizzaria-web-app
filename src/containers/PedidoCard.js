import React from 'react';
import { Link } from 'react-router-dom';

import accounting from '../utils/formatMoney';

import {
    PedidoContainer,
    PedidoContainerTitle,
    PedidoContainerPriceTag,
    PedidoContainerCount,
    PedidoType,
    PedidoSection,
    AdicionaisTitle,
    AdicionaisPrice,
    ValorTotalIndividual,
    PedidoHeader,
    PartesContainer,
} from '../styled-components/pedido';

import { Divider } from '../styled-components/divider';

export const PedidoCard = props => {

    const path = `/detalhe/${props.produto}/${props.id.toLowerCase()}`;
    const { type, title, counter, value, adicionais, partes, borda, totalValue, observacao } = props;

    return (
        <Link to={path} onClick={props.disabledLink ? e => e.preventDefault() : null}>
            <PedidoContainer>
                <PedidoType>{type}</PedidoType>
                <PedidoHeader>
                    <PedidoContainerTitle>
                        {`${title}`}
                        <PedidoContainerCount>x{counter}</PedidoContainerCount>
                    </PedidoContainerTitle>
                    <PedidoContainerPriceTag>
                        {accounting.formatMoney(value)}
                    </PedidoContainerPriceTag>
                </PedidoHeader>
                {adicionais.length > 0
                    ? <PedidoSection>
                        <Divider />
                        Adicionais
                    {adicionais.map(adicional => (
                            <AdicionaisTitle
                                key={adicional.id}
                            >{adicional.name}
                                <AdicionaisPrice >{accounting.formatMoney(adicional.value)}</AdicionaisPrice>
                            </AdicionaisTitle>))}
                    </PedidoSection>
                    : null
                }
                {partes.length > 0
                    ? <PedidoSection>
                        <Divider />
                        Partes
                    {partes.map(parte => <PartesContainer key={parte.id}>{parte.name}

                            {parte.adicionais.length > 0
                                ? <PedidoSection>
                                    <Divider />
                                    Adicionais para esta parte
                    {parte.adicionais.map(adicional => (
                                        <AdicionaisTitle
                                            key={adicional.id}
                                        >{adicional.name}
                                            <AdicionaisPrice >{accounting.formatMoney(adicional.value)}</AdicionaisPrice>
                                        </AdicionaisTitle>))}
                                </PedidoSection>
                                : null
                            }
                        </PartesContainer>)}
                    </PedidoSection>
                    : null}
                {borda.id
                    ? <PedidoSection>
                        <Divider />
                        Borda
                <AdicionaisTitle key={borda.id}>{borda.name}
                            <AdicionaisPrice >{accounting.formatMoney(borda.value)}</AdicionaisPrice>
                        </AdicionaisTitle>
                    </PedidoSection>
                    : null}
                {observacao
                    ? <PedidoSection>
                        Observações:
                        <p>{observacao}</p>
                    </PedidoSection> : null}
                <Divider />
                <ValorTotalIndividual>
                    Valor total deste item:
                    <PedidoContainerPriceTag>{accounting.formatMoney(totalValue)}</PedidoContainerPriceTag>
                </ValorTotalIndividual>
            </PedidoContainer>
        </Link>
    )
};
