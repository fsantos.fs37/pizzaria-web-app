import styled from 'styled-components';
import { color } from './colors';

export const StyledHeaderContainer = styled.header`
    width: 100%;
    position: fixed;
    top: 0;
    margin: 0 auto 20px;
    background: ${color.backgroundGreenLight};
    border-bottom: 1px solid rgba(0,0,0,.1); 
    z-index: 999;
    transform: ${props => props.visible ? 'translateY(0)' : 'translateY(-102%)'};
    transition: transform .3s ease-in-out;
    transition-delay: .3s;
`;

export const StyledHeader = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    max-width: 600px;
    min-height: 59px;
    margin: auto;
    padding: 0 10px;
`


