import styled from "styled-components";

export const CounterButtonContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center
`

export const CounterButton = styled.button`
    width: 25px;
    height: 25px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: transparent;
    border: none;
    border-radius: 100%;
    cursor: pointer;

    .material-icons {
        font-size: 18px;
        color: rgba(0,0,0,.8);
    }
`

export const CounterValue = styled.input`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 50px;
    height: 35px;
    text-align: center;
    font-size: 14px;
    font-weight: 500;
    margin: 0 10px;
    padding: 0 10px;
    background: transparent;
    border: 1px solid rgba(0,0,0,.8);
    border-radius: 5px;
    color: rgba(0,0,0,.8);
`
