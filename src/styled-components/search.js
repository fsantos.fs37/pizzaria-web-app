import styled from 'styled-components';
import { color } from './colors';
import { typography } from './typography';

export const SearchInput = styled.input`
    width: 100%;
    padding: 10px 5px 10px;
    border: none;
    border-radius: 5px;
    border: none;
    margin: 0 0 10px;
    background: #fff176;
    border: 1px solid rgba(0,0,0,.1);
    border-radius: 5px;
    font-family: ${typography.body};
    color: ${color.primary};
    outline: ${color.primary};
    font-size: 17px;

    &::placeholder {
        color: #000
    }

`

export const SearchForm = styled.form`
    width: 100%;
    background: ${color.backgroundGreenLight};
    border-radius: 10px;
    padding: 10px;
    margin:  0 0 20px 0;
    font-family: ${typography.body};
`

export const SearchInputRadio = styled.input`
    height: 15px;
    width: 15px;
    padding: 0;
    border: none;
    border-radius: 5px;
    border: none;
    margin:  0 10px 0 0;
    background: ${color.backgroundGreenLight};;
    font-family: ${typography.body};
    color: rgba(0,0,0,.8);
    outline: ${color.primary};
    font-size: 17px;
`

export const SearchLabel = styled.label`
    width: 100%;
    display: flex;
    align-items: center;
    font-family: ${typography.body};
    font-size: 14px;
    color: ${color.primary};
    margin-top: 10px
`


