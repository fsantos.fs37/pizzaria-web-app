import styled from 'styled-components';

export const AccordionPane = styled.div`
    width: 100%
    position: relative;
    border-radius: 5px;
    margin-bottom: 10px

    &:last-child {
        margin-bottom: 0
    }

`

export const AccordionButton = styled.div`
    width: 100%;
    color: #ffffff;
    text-align: center;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0;
    border: none;
    border-radius: 5px;
    background: transparent;
`

export const AccordionBody = styled.div`
    width:  100%;
    height: 0px;
    transition: all .3s ease;
    overflow: hidden;
    background: transparent;
`
