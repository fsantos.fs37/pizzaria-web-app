import styled from 'styled-components';
import { color } from './colors';
import { typography } from './typography';

export const ProfileContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
`

export const Avatar = styled.img`
    width: 75px;
    height: 75px;   
    margin: 20px auto 0;
    border-radius: 100px;
`

export const UserName = styled.p`
    width: 100%;
    margin: 10px auto;
    text-align: center;
    font-size: 14px;
    color: ${color.light};
    font-family: ${typography.head};
`

