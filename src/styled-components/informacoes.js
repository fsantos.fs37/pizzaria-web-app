import styled from 'styled-components';
import { typography } from './typography';


export const InformacoesSection = styled.div`
    margin-top: 30px;
`

export const InformacoesContainer = styled.div`
    width: 100%;
    margin: auto auto 10px;
    padding: 10px;
    border: 1px solid rgba(0,0,0,.1);
    border-radius: 10px;
    background: #ffffff;
    font-family: ${typography.body};
`

export const InformacoesTitle = styled.div`
    text-align: ${props => props.center && 'center'}
    font-size:${props => props.small ? '14px' : '18px'};
    margin-bottom: ${props => props.bottom && '20px'}
    font-weight: 700;
    color: #000
`

export const InformacoesSubtitle = styled.div`
    text-align: ${props => props.center && 'center'}
    font-size: 14px;
    font-weight: 600;
    color: #000
`

export const InformacoesLink = styled.a`
    text-decoration: none;
    color: #2196F3
`
