import styled from "styled-components";
import { color } from "./colors";
import { typography } from "./typography";

export const StyledHeaderButton = styled.button`
  padding: 0;
  border: none;
  border-radius: 5px;
  background: transparent;
  cursor: pointer;
  outline: none;
  font-family: ${typography.head};
  font-size: ${props => (props.large ? "18px" : "16px")};
  color: rgba(0, 0, 0, 0.8);

  .material-icons {
    color: ${color.primary};
    font-size: ${props => (props.large ? "24px" : "18px")};
  }
`;

export const OrderButton = styled.button`
  padding: 7px 0;
  margin: 0 6px;
  border: none;
  border-radius: 5px;
  background: transparent;
  color: ${color.primary};
  font-family: ${typography.body};
  font-weight: 600;
  cursor: pointer;
  outline: none;
`;

export const OrderButtonContainer = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  background: ${props =>
    props.transparent ? "transparent" : color.backgroundGreenLight};
  border: none;
  border-radius: 5px;
  padding: 15px 10px;
  margin: 0 0 10px;
  cursor: pointer;
`;

export const OrderButtonTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  font-size: 21px;
  font-weight: 700;
  color: ${color.primary};
  font-family: ${typography.body};
  margin: 0 0 5px;
  text-align: left;
`;

export const OrderButtonDescription = styled.p`
  text-align: left;
  font-size: 14px;
  font-weight: 500;
  margin: 0;
  color: ${color.primary};
  font-family: ${typography.body};
`;

export const PriceTag = styled.p`
  font-size: 16px;
  font-weight: 700;
  margin: 0;
  padding: 2px 9px 0;
  border-radius: 3px;
  color: ${color.secondary};
  font-family: ${typography.body};
`;

export const PriceButton = styled.div`
  color: ${color.secondary};
  padding: 5px 8px;
  font-size: 18px;
  border-radius: 3px;
  font-weight: 600;
  font-family: "Lekton", sans-serif;
`;

export const ConfirmOrderButtonContainer = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: space-between;
`;

export const ConfirmOrderButton = styled.button`
  max-width: 220px;
  width: 100%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  text-align: center;
  background: ${props =>
    props.filled ? color.backgroundGreenLight : "transparent"};
  color: ${props => (props.white ? "#ffffff" : color.primary)};
  border: none;
  border-radius: 5px;
  padding: 10px 10px;
  margin: 0;
  align-self: flex-end;
  cursor: pointer;
  font-size: 14px;
  font-family: ${typography.body};
  font-weight: 600;

  .material-icons {
    color: #356859;
  }
`;

export const StyledButton = styled.button`
  max-width: 100%;
  width: 100%;
  display: flex;
  justify-content: ${props => (props.left ? "" : "space-around")};
  align-items: center;
  text-align: center;
  background: ${({ unFilled }) =>
    unFilled ? "transparent" : color.backgroundGreenLight};
  color: ${
  props => {
    if (props.unFilled) return color.light;
    else if (props.white) return '#fff';
    else if (props.danger) return 'tomato';
    else return color.primary;
  }
  } 
  border: none;
  border-radius: 5px;
  padding: ${props => (props.noPadding ? "0" : "15px 10px")};
  margin: ${props => (props.noMargin ? "0" : "10px  auto")};
  cursor: pointer;
  font-size: 14px;
  font-family: ${typography.body};
  font-weight: 600;

  & .material-icons {
    margin: 0 30px 0 20px;
  }

  & .button-icon {
    margin: 0 30px 0 20px;
    width: 24px;
  }
`;
