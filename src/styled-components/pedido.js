import styled from 'styled-components';
import { color } from './colors';
import { typography } from './typography';

export const PedidoContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    background: ${color.backgroundGreenLight};
    border-radius: 5px;
    padding: 16px 10px;
    margin: 20px 0 0 0;
    font-size: 14px;
    position: relative;
`
export const PedidoContainerTitle = styled.div`
    display: flex;
    align-items: center;
    font-size: 20px;
    font-weight: 600;
    font-family: 'Lekton', sans-serif;
    color: rgba(0,0,0,.8)
`

export const PedidoContainerCount = styled.div`
    font-weight: 400;
    margin: 0 10px;
    border-radius: 100px;
    width: 25px;
    height: 25px;
    display: flex;
    justify-content: center;
    background: #fff176;
    align-items: center;
    color: rgb(0,0,0);
    font-size: 12px;
    font-family: 'Lekton', sans-serif;
    
`

export const PedidoContainerPriceTag = styled.p`
    font-size: 16px;
    font-weight: 600;
    margin: 0;
    padding: 0 9px;
    border-radius: 3px;
    font-family: 'Lekton', sans-serif;
    color: #fd5523
`

export const PedidoType = styled.div`
    font-size: 14px;
    font-weight: 600;
    text-transform: capitalize;
    margin-bottom: 5px;
    font-family: 'Lekton', sans-serif;
    color: rgba(0,0,0,.8)
`

export const PedidoSection = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    font-size: 18px;
    font-weight: 600;
    text-transform: capitalize;
    margin: 10px 0 ;
    font-family: 'Lekton', sans-serif;
    color: rgba(0,0,0,.8)
`

export const PedidoHeader = styled.header`
    width: 100%;
    display: flex
    justify-content: space-between;
    align-items: center;
    margin: 10px auto;
`


export const AdicionaisTitle = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: 14px;
    font-weight: 600;
    height: 30px;
    text-transform: capitalize;
    font-family: 'Lekton', sans-serif;
    color: rgba(0,0,0,.8)
`

export const PartesContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    text-align: left;
    align-items: flex-start;
`


export const AdicionaisPrice = styled.p`
    font-size: 14px;
    font-weight: 600;
    text-transform: capitalize;
    margin-bottom: 5px;
    font-family: ${typography.body};
    color: ${color.secondary}
`

export const FinalizarPedidoButton = styled.button`
    width: 100%;
    display: flex;
    justify-content: ${({ center }) => center ? 'center' : 'space-between'};
    align-items: center;
    text-align: center;
    background: ${props => props.filled ? color.backgroundGreenLight : 'transparent'};
    color: ${color.primary};
    border: none;
    border-radius: 5px;
    padding: 15px 10px;
    margin: ${props => props.center ? '30px auto 0' : '30px 0 0'};
    align-self: flex-end;
    cursor: pointer;
    font-size: 14px;
    font-family: ${typography.body};
    font-weight: 600;
`

export const ValorTotalIndividual = styled.div`
    width: 100%;    
    display: flex;
    justify-content: space-between;
    align-items: center;
    color: ${color.primary};
    font-family: ${typography.body};
    font-size: 14px;
    font-weight: 600;
`

export const EmptyOrderMessage = styled.p`
    color: ${color.light};
    font-family: ${typography.body};
    font-size: 14px;
`

export const StatusButton = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    text-align: center;
    background: ${color.backgroundGreenLight};
    color: ${color.primary};
    border: none;
    border-radius: 5px;
    padding: 15px 10px;
    margin:30px auto 0;
    align-self: flex-end;
    cursor: pointer;
    font-size: 17px;
    font-family: ${typography.body};
    font-weight: 600;
`

export const StatusTag = styled.div`
    font-size: 17px;
    font-weight: 600;
    margin: 0;
    padding: 2px 0 0;
    border-radius: 3px;
    color: ${color.secondary};
    font-family: ${typography.body};
`

