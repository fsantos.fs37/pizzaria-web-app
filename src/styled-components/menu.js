import styled from "styled-components";
import { color } from "./colors";

export const MenuContainer = styled.nav`
    margin: 50px 0 0;
    width: 100%; 
`
export const MenuItem = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    height: 65px;
    padding: 10px;
    padding-left: ${props => props.isChild ? '20px' : null};
    margin: ${props => props.isChild ? '10px 0 0' : '0'}; 
    background: ${props => props.isChild ? '#fff176' : color.backgroundGreenLight};
    color: rgba(0,0,0,.8);
    border-radius: 2px; 
    font-family: 'Lekton', sans-serif;
    font-weight: ${props => props.isChild ? '400' : '400'};;
    text-align: left;
    cursor: pointer;
    font-size: 16px;
    font-weight: 700;

    .material-icons {
        font-size: 28px;
        color: rgba(0,0,0,.8);
    };

`

export const AutoMenuItem = styled.button`
    display: flex;
    align-items: center;
    width: 100%;
    height: 65px;
    padding: 10px;
    padding-left: ${props => props.isChild ? '30px' : null};
    margin: 0 0 10px 0; 
    background: #ffffff;
    color: #e53935;
    border-radius: 5px; 
    font-weight: ${props => props.isChild ? '400' : '400'};;
    text-align: left;
    cursor: pointer;
    font-size: 16px;

    .material-icons {
        font-size: 28px;
    };

`

