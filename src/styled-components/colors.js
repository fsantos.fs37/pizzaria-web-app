//Cores
const primary = '#000';
const secondary = '#FD5523';
const light = '#ffffff';
const backgroundGreenLight = '#ffeb3b';

export const color = {
    primary,
    secondary,
    light,
    backgroundGreenLight
}