import styled from 'styled-components';

export const StyledCard = styled.div`
    max-width: 100px;
    width: 100%;
    padding: 10px;
    margin: 15px 10px;
    min-height: 150px;
    background: #ffffff;
    border-radius: 10px;
    border: 1px solid rgba(0,0,0,.1);
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
`

export const StyledImg = styled.img`
    width: 100px;
`