import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import registerServiceWorker from './registerServiceWorker';
import store from './state/store';
import { Provider } from 'react-redux';

if (window.location.port !== '3000') {
    console.log('Orgulhosamente criado por https://frankzang.github.io');
}

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    , document.getElementById('root'));
// registerServiceWorker();
