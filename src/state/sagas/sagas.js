import { put, takeEvery, takeLatest, all, call } from 'redux-saga/effects';
import { database, auth } from '../../firebase/firebase';
import {
    actions,
    WATCH_GET_PROMO_PRODUCTS,
    GET_PROMO_PRODUCTS_SUCCESS,
    UPDATE_HORARIO,
    NEW_HORARIO,
    NEW_STAGE_ORDER,
    UPDATE_STAGE_ORDER,
    GET_ORDER,
    GET_ORDER_WATCHER
} from '../actions/actions';

import { watchUpdateAdress, watchUpdatePaymentMethod, watchUpdateUserContact } from './adressAndPaymentSagas';
import { watchOrderSubmitted, watchReceivedOrderId, watchOrderStatusChanged } from './orderRequestSaga';
import { watchCheckForUser } from './authSagas';

//Saga para pegar todos os produtos da base de dados;
export function* getPizzasFromDatabase() {
    if (!window.localStorage.getItem('produtos')) {
        try {
            const pizzasCollection = database.collection('pizzas');
            const bebidasCollection = database.collection('bebidas');
            const esfihasCollection = database.collection('esfihas');
            const adicionaisCollection = database.collection('adicionais');
            const bordasCollection = database.collection('bordas');

            const [pizzas, bebidas, esfihas, adicionais, bordas] = yield all([
                call(() => pizzasCollection.get()),
                call(() => bebidasCollection.get()),
                call(() => esfihasCollection.get()),
                call(() => adicionaisCollection.get()),
                call(() => bordasCollection.get())
            ])

            const extractDocumentData = documents => {
                const items = [];
                documents.forEach(document => {
                    const documentData = {
                        id: document.id.toLowerCase(),
                        ...document.data()
                    };
                    items.push(documentData);
                });
                return items;
            }

            const produtos = {
                pizzas: extractDocumentData(pizzas),
                esfihas: extractDocumentData(esfihas),
                bebidas: extractDocumentData(bebidas),
                adicionais: extractDocumentData(adicionais),
                bordas: extractDocumentData(bordas)
            };

            localStorage.setItem('produtos', JSON.stringify(produtos));
            yield put({ type: actions.FETCH_PIZZAS_SUCCESS, payload: produtos });
        }
        catch (err) {
            console.log(err);
        }
    }
    else {
        yield put({ type: actions.FETCH_PIZZAS_SUCCESS, payload: JSON.parse(window.localStorage.getItem('produtos')) });
    }
}
export function* watchGetPizzasFromDatabase() {
    yield takeEvery(actions.FETCH_PIZZAS_FROM_FIRESTORE, getPizzasFromDatabase)
}

export function* updatePrice(action) {
    const { payload } = action;
    yield put({ type: actions.UPDATE_PRICE, payload })
}
export function* watchUpdatePrice() {
    yield takeLatest(actions.NEW_UPDATED_PRICE, updatePrice)
}

export function* updateOrder(action) {
    const { payload } = action;
    yield put({ type: actions.UPDATE_ORDER, payload })
    yield put({ type: actions.UPDATE_PRICE, payload })
}
export function* watchUpdateOrder() {
    yield takeLatest(actions.NEW_ORDER, updateOrder)
}

export function* removeOrder(action) {
    const { payload } = action;
    yield put({ type: actions.REMOVE_ORDER, payload })
    yield put({ type: actions.UPDATE_PRICE, payload })
}
export function* watchRemoveOrder() {
    yield takeLatest(actions.NEW_REMOVE_ORDER, removeOrder)
}

export function* incrementCounter() {
    yield put({ type: actions.INCREMENT_COUNTER })
}
export function* watchIncrementCounter() {
    yield takeLatest(actions.UPDATE_INCREMENT_COUNTER, incrementCounter)
}

export function* decrementCounter() {
    yield put({ type: actions.DECREMENT_COUNTER })
}
export function* watchDecrementCounter() {
    yield takeLatest(actions.UPDATE_DECREMENT_COUNTER, decrementCounter)
}

export function* changeCounter(action) {
    const { payload } = action;
    yield put({ type: actions.CHANGE_COUNTER, payload })
}
export function* watchChangeCounter() {
    yield takeLatest(actions.UPDATE_CHANGE_COUNTER, changeCounter)
}

export function* changeParts(action) {
    const { payload } = action;
    yield put({ type: actions.CHANGE_PARTS, payload })
}
export function* watchChangeParts() {
    yield takeLatest(actions.UPDATE_CHANGE_PARTS, changeParts)
}

export function* changeProduct(action) {
    const { payload } = action;
    yield put({ type: actions.CHANGE_PRODUCT, payload })
}
export function* watchChangeProduct() {
    yield takeLatest(actions.UPDATE_CHANGE_PRODUCT, changeProduct)
}

export function* changeAdicionais(action) {
    const { payload } = action;
    yield put({ type: actions.CHANGE_ADICIONAIS, payload })
}
export function* watchChangeAdicionais() {
    yield takeLatest(actions.UPDATE_CHANGE_ADICIONAIS, changeAdicionais)
}

export function* resetCartReducer() {
    yield put({ type: actions.RESET_CART_REDUCER })
}
export function* watchResetCartReducer() {
    yield takeLatest(actions.NEW_RESET_CART_REDUCER, resetCartReducer)
}

export function* addCurrentCartOptions(action) {
    const { payload } = action;
    yield put({ type: actions.CURRENT_CART_OPTIONS, payload })
}
export function* watchAddCurrentCartOptions() {
    yield takeLatest(actions.NEW_CURRENT_CART_OPTIONS, addCurrentCartOptions)
}

export function* addToStageCart() {
    yield put({ type: NEW_STAGE_ORDER })
}
export function* watchAddToStageCart() {
    yield takeLatest(UPDATE_STAGE_ORDER, addToStageCart)
}

export function* updatePartsOptions(action) {
    const { payload } = action;
    yield put({ type: actions.UPDATE_PARTS_OPTIONS, payload })
}
export function* watchUpdatePartsOptions() {
    yield takeLatest(actions.NEW_UPDATE_PARTS_OPTIONS, updatePartsOptions)
}

export function* updateBordasOptions(action) {
    const { payload } = action;
    yield put({ type: actions.UPDATE_BORDAS_OPTIONS, payload })
}
export function* watchUpdateBordasOptions() {
    yield takeLatest(actions.NEW_UPDATE_BORDAS_OPTIONS, updateBordasOptions)
}

export function* newObservacao(action) {
    const { payload } = action;
    yield put({ type: actions.NEW_OBSERVACAO, payload })
}
export function* watchNewObservacao() {
    yield takeLatest(actions.UPDATE_OBSERVACAO, newObservacao)
}

export function* newHorario() {
    const request = yield database.collection('pizzaria').doc('horario').get();
    const horario = yield request.data().horario;
    yield put({ type: NEW_HORARIO, payload: horario })
}
export function* watchNewHorario() {
    yield takeLatest(UPDATE_HORARIO, newHorario)
}

export function* getOrdersNumber() {
    yield auth.getRedirectResult();
    if (auth.currentUser) {
        const request = yield database.collection('users')
            .doc(auth.currentUser.uid).get();
        const pedidos = request.data().pedidosArray;
        if (pedidos) {
            yield put({ type: GET_ORDER, payload: pedidos })

        }
    }
}
export function* watchgetOrdersNumber() {
    yield takeLatest(GET_ORDER_WATCHER, getOrdersNumber)
}

export function* getPromoProducts() {
    const response = yield database.collection('promocoes').doc('pizzas').get();
    const data = response.data().combo;
    console.log(data)
    yield put({ type: GET_PROMO_PRODUCTS_SUCCESS, payload: data });
}
export function* watchGetPromoProducts() {
    yield takeLatest(WATCH_GET_PROMO_PRODUCTS, getPromoProducts)
}

// Todas as ações que chamarão as sagas são unificadas numa única "root saga".
export default function* rootSaga() {
    yield all([
        watchGetPizzasFromDatabase(),
        watchUpdatePrice(),
        watchUpdateOrder(),
        watchRemoveOrder(),
        watchIncrementCounter(),
        watchDecrementCounter(),
        watchChangeParts(),
        watchChangeCounter(),
        watchChangeProduct(),
        watchChangeAdicionais(),
        watchResetCartReducer(),
        watchAddCurrentCartOptions(),
        watchUpdatePartsOptions(),
        watchUpdateBordasOptions(),
        watchUpdateAdress(),
        watchUpdatePaymentMethod(),
        watchOrderSubmitted(),
        watchCheckForUser(),
        watchUpdateUserContact(),
        watchReceivedOrderId(),
        watchOrderStatusChanged(),
        watchNewObservacao(),
        watchNewHorario(),
        watchAddToStageCart(),
        watchgetOrdersNumber(),
        watchGetPromoProducts(),
    ])
}