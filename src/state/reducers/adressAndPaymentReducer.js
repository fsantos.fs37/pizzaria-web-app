import { UPDATE_ADRESS, UPDATE_PAYMENT_METHOD } from '../actions/actions';

const initialState = {
    adress: {},
    payment: {},
}

export default function adressAndCartReducer(state = initialState, action) {
    switch (action.type) {
        case UPDATE_ADRESS:
            return state = { ...state, adress: action.payload };
        case UPDATE_PAYMENT_METHOD:
            return state = { ...state, payment: action.payload };
        default:
            return state;
    }
}
